SUB_SRC	=	pre.cpp check.cpp click.cpp find.cpp main.cpp
SUB_O	=	pre.o check.o click.o find.o main.o
# MLIBS	=	-lMagick++-7.Q16HDRI -lMagickCore-7.Q16HDRI -lMagickWand-7.Q16HDRI

all: main

	# g++ -o main pre.cpp check.cpp click.cpp find.cpp main.cpp -lxdo  `Magick++-config --cppflags --cxxflags --ldflags --libs`
main: $(SUB_O)
	g++ -o $@ $? -Llibs -lxdo -lMagick++-7 -lscreenshot
pre.o: pre.cpp
	g++ $< -c -Iinclude -Iinclude -fopenmp -DMAGICKCORE_HDRI_ENABLE=1 -DMAGICKCORE_QUANTUM_DEPTH=16 
check.o: check.cpp 
	g++ $< -c -Iinclude -Iinclude -fopenmp -DMAGICKCORE_HDRI_ENABLE=1 -DMAGICKCORE_QUANTUM_DEPTH=16
click.o: click.cpp 
	g++ $< -c -Iinclude
find.o: find.cpp 
	g++ $< -c -Iinclude
main.o: main.cpp 
	g++ $< -c -Iinclude -Iinclude -fopenmp -DMAGICKCORE_HDRI_ENABLE=1 -DMAGICKCORE_QUANTUM_DEPTH=16
clear:
	rm -rf $(SUB_O)