#include "pre.h"
#include "check.h"
#include "click.h"
#include "find.h"

int main(int argc, char const *argv[])
{
	bool checked;
	int allCheck = 0;
	Check c;
	Find f;
	sleep(3);
	f.find();
	printf("%d\t%d\n", f.x, f.y);
	for (int i = 0; i < 10000; i++) {
		Pre p;
		Click cl;
		p.crop(f.x, f.y);
		p.colores();
		p.resize();
		sleep(0.5);
		checked = c.check(p.image);
		if (checked) {
			// std::cout << "Fish!" << std::endl;
			cl.click();
			allCheck++;
			sleep(4);
		}
		else allCheck = 0;
		if (allCheck > 30) {
			printf("%d\n", i);
			exit(1);
		}
	}
	return 0;
}