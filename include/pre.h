#include <string>
#include <Magick++.h>
#include <iostream>
#include <unistd.h>

// using namespace Magick;

class Pre {
	private:
		int w = 100,
			h = 115,
			x = 920,
			y = 270;
    public:
        Magick::Image image;
        Pre(std::string); 
    	Pre(void);
        void crop();
    	void crop(int, int);
    	void colores();
    	void resize();
    	void show();
};