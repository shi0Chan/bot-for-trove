#include <stdio.h>
#include <stdlib.h>
extern "C" {
#include <xdo.h>
}
#include <cstring>
#include <iostream>
#include <string>

class Click {
	private:
		xdo_t * x = xdo_new(":0.0");
	public:
		int id;
		Click();
		int getId();
		void click();
		~Click();
};