#include "click.h"
Click::Click() {
	id = getId();
}

int Click::getId() {
	Window *list;
	xdo_search_t search;
	unsigned int nwindows;
	memset(&search, 0, sizeof(xdo_search_t));
	search.max_depth = -1;
	search.require = xdo_search::SEARCH_ANY;
	
	search.searchmask |= SEARCH_NAME;
	search.winname = "Trove";
	
	int id = xdo_search_windows(x, &search, &list, &nwindows);
	return id;
};
void Click::click() {
	const char* ch = "f";
	xdo_send_keysequence_window(x, id, ch, 0);
};
Click::~Click() {
	xdo_free(x);
};