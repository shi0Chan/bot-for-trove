#include "pre.h"
extern "C" {
    #include "screenshot.h"
}


Pre::Pre(void) {
    Magick::InitializeMagick("");
    // system("scrot -a 0,0,1920,1080 /tmp/temp.png");
    char filename[] = "/tmp/temp.png";
    get_screenshot(0,0,1920,1080,filename);
    image.read("/tmp/temp.png");
    // image.read("x:0x1e0");
};
Pre::Pre(std::string filename) {
    Magick::InitializeMagick("");
    image.read(filename);
};

void Pre::crop() {
    std::string coord = std::to_string(w) + "x" + std::to_string(h) + "+" + std::to_string(x) + "+" + std::to_string(y);
    Magick::Geometry cropping(coord);
    image.crop(cropping);
};

void Pre::crop(int Tx, int Ty) {
    x = Tx-w/2;
    y = Ty-h/2;
    std::string coord = std::to_string(w) + "x" + std::to_string(h) + "+" + std::to_string(x) + "+" + std::to_string(y);
    Magick::Geometry cropping(coord);
    image.crop(cropping);
};
void Pre::colores() {
    image.colorSpace(Magick::GRAYColorspace);
    image.sigmoidalContrast(1,255);
};
void Pre::resize() {
    image.scale(Magick::Geometry("20x23"));
};
void Pre::show() {
    image.display();
};



    // image.display();
    // image.write("out2.png");

