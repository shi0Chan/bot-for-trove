#include "check.h"

bool Check::check(Magick::Image image) {
	_height = image.rows();
	_width = image.columns();

    total = _height * _width;
    white = 0;

    for ( unsigned int y = 0; y < _height; y++ ) {
		for ( unsigned int x = 0; x < _width; x++ ) {
	
			Magick::Color c = image.pixelColor( x, y );
			int r = c.quantumRed() * 255 / QuantumRange;
			int g = c.quantumGreen() * 255 / QuantumRange;
			int b = c.quantumBlue() * 255 / QuantumRange;
			if (r+b+g == 765) white++;
		}	
	}
	if (total < white+(total*0.2))
		return true;
	// else return false;
	else return false;
}
